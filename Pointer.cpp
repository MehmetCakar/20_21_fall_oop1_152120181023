#include <iostream>
#include <stdio.h>
#include<math.h>
void update(int *a, int *b) {
	// Complete this function  
	int c = *a;//! making the temporary integer to assaign pointer a.
	*a = *a + *b;//! collect two integer pointer and then assaign them inside pointer a
	*b = abs(c - *b);//! extract two integer pointer and then assaign them inside pointer b
}

int main() {
	int a, b;
	int *pa = &a, *pb = &b;

	scanf("%d %d", &a, &b);
	update(pa, pb);
	printf("%d\n%d", a, b);

	return 0;
}
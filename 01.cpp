#include<iostream>
#include<stdlib.h>
#include<math.h>
#include<fstream>
#include<string>
using namespace std;

int sum1(int[], int);
int product1(int[], int);
int average(int[], int);
int smallest(int[], int);
int main() {

	fstream newData, fakeData;
	string nameofFile;
	int q,qq,count=0;
	int *dizi,*dizi1;
	int a = 0;
	cout << "Please enter name of the file that you want to open: ";
	getline(cin, nameofFile);
	newData.open(nameofFile, ios::in);
	fakeData.open(nameofFile, ios::in);
	if (!newData) { //! Checking open the file.
		cout << "Error!File can not be opened! Plaese check your own code!";
		exit(0);
	}

	newData >> q;
	fakeData >> qq;
	dizi = new int[q];
	dizi1 = new int[qq];
	for (int i = 0; i < q; i++)
	{
		newData >> dizi[i];
	}
	newData.close();


	if (!fakeData) { //! Checking open the file.
		cout << "Error!File can not be opened! Plaese check your own code!"<<endl;
		system("pause");
		exit(0);
	}
	
	int ii = 0;
	while (!fakeData.eof()) {

		fakeData >> dizi1[ii];
		ii++;
		count++;
	}
	if (ii != q) { //! Checking matching
		cout << "Error!Numbers are not match each other! Plaese check your own code!"<<endl;
		system("pause");
		exit(0);
	}
	fakeData.close();

	
	cout << endl << ii <<endl<< count << endl;

	a = sum1(dizi, q);
	cout <<"Sum is " <<a << endl;

	a = product1(dizi, q);
	cout <<"Product is "<< a << endl;

	a = average(dizi, q);
	cout <<"Average is "<< a << endl;

	a = smallest(dizi, q);
	cout <<"Smallest is "<< a << endl;
	system("pause");
	return 0;
}
int sum1(int x[], int y) {
	int sum = 0;
	for (int i = 0; i < y; i++)
	{
		sum = sum + x[i];
	}
	return sum;
}
int product1(int x[], int y) {
	int pro = 1;
	for (int i = 0; i < y; i++)
	{
		pro = pro*x[i];
	}
	return pro;
}
int average(int x[], int y){
	int a = 0;
	int avg = 0;
	a = sum1(x, y);
	avg = a / y;
	return avg;
}
int smallest(int x[], int y){
	int small = x[0];
	for (int i = 0; i < y; i++)
	{
		
		if (x[i] > x[i + 1]) {
			if (i == y-1)
				break;

			small = x[i + 1];
		}
	}
	return small;
}
#include <iostream>
#include <sstream>
using namespace std;

/*
Enter code for class Student here.
Read statement for specification.
*/
class Student {
private://! private members
	int age;
	string first_name;
	string last_name;
	int standard;
public:


	void set_age(int x) {//! assaign the age
		age = x;
	}
	int get_age() {//! return the age
		return age;
	}
	void set_standard(int x) {//! assaign the standard
		standard = x;
	}
	int get_standard() {//! return the standard
		return standard;
	}
	void set_first_name(string x) {//! assaign the first name
		first_name = x;
	}
	string get_first_name() //! return the first name
		return first_name;
	}
	void set_last_name(string x) //! assaign the last name
		last_name = x;
	}
	string get_last_name() //! return the last name
		return last_name;
	}
	string to_string() { //! Converting integer to string 
		ostringstream strg, strg1;
		strg << age;
		strg1 << standard;
		string s1 = strg.str();
		string s2 = strg1.str();

		return s1 + "," + first_name + "," + last_name + "," + s2;
	}

};
int main() {
	int age, standard;
	string first_name, last_name;

	cin >> age >> first_name >> last_name >> standard;

	Student st;
	st.set_age(age);
	st.set_standard(standard);
	st.set_first_name(first_name);
	st.set_last_name(last_name);

	cout << st.get_age() << "\n";
	cout << st.get_last_name() << ", " << st.get_first_name() << "\n";
	cout << st.get_standard() << "\n";
	cout << "\n";
	cout << st.to_string();

	return 0;
}

#include<bits/stdc++.h>

using namespace std;
//Implement the class Box  
//l,b,h are integers representing the dimensions of the box

// The class should have the following functions : 

// Constructors:
class Box {
private: // private members
	int length;
	int breadth;
	int height;
public:
	Box() {// Constructors:
		length = 0;
		breadth = 0;
		height = 0;
	}
	Box(int l, int b, int h) {//! Assaign the numbers from main to private members
		length = l; 
		breadth = b;
		height = h;
	}
	Box(Box &B) {//! Copy Constructors
		this->length = B.length;
		this->height = B.height;
		this->breadth = B.breadth;
	}
	int getLength() { return length; } // Return box's length
	int getBreadth() { return breadth; } // Return box's breadth
	int getHeight() { return height; }  //Return box's height
	long long int CalculateVolume() {
		return (long long int)height*(long long int)breadth*(long long int)length;
	}
	bool operator<(Box &B) {//!Overload operator < as specified

		if (this->length<B.length) {
			return true;
		}
		else if (this->breadth<B.breadth && this->length == B.length) {
			return true;
		}
		else if (this->height<B.height && this->breadth == B.breadth && this->length == B.length) {
			return true;
		}
		else {
			return false;
		}
	}

	friend ostream& operator<<(ostream& out, Box& B) {//!Overload operator < as specified
		out << B.length << " " << B.breadth << " " << B.height;//!ostream& operator<<(ostream& out, Box& B)
		return out;
	}

};

//bool operator<(Box& b)

//Overload operator << as specified
//ostream& operator<<(ostream& out, Box& B)


void check2()
{
	int n;
	cin >> n;
	Box temp;
	for (int i = 0;i<n;i++)
	{
		int type;
		cin >> type;
		if (type == 1)
		{
			cout << temp << endl;
		}
		if (type == 2)
		{
			int l, b, h;
			cin >> l >> b >> h;
			Box NewBox(l, b, h);
			temp = NewBox;
			cout << temp << endl;
		}
		if (type == 3)
		{
			int l, b, h;
			cin >> l >> b >> h;
			Box NewBox(l, b, h);
			if (NewBox<temp)
			{
				cout << "Lesser\n";
			}
			else
			{
				cout << "Greater\n";
			}
		}
		if (type == 4)
		{
			cout << temp.CalculateVolume() << endl;
		}
		if (type == 5)
		{
			Box NewBox(temp);
			cout << NewBox << endl;
		}

	}
}

int main()
{
	check2();
}